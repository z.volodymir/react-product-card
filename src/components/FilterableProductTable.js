import React from "react";

import SearchBar from "./SearchBar";
import ProductTable from "./ProductTable";

export default class FilterableProductTable extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			filterText: '',
			inStockOnly: false
		};
		this.productFilter = this.productFilter.bind(this);
		this.toggleStock = this.toggleStock.bind(this);
	}

	productFilter(search) {
		this.setState(() => {
			return {
				filterText: search
			}
		});
	}

	toggleStock(checked) {
		this.setState(() => {
			return{
				inStockOnly: checked
			}
		})
	}

	render() {
		const {filterText, inStockOnly} = this.state;

		return (
			<div>
				<SearchBar
					productFilter={this.productFilter}
					toggleStock={this.toggleStock}
				/>
				<ProductTable
					products={this.props.products}
					filterText={filterText}
					inStockOnly={inStockOnly}
				/>
			</div>
		);
	}
}