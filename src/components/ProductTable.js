import React from "react";

import ProductCategoryRow from "./ProductCategoryRow";
import ProductRow from "./ProductRow";

export default class ProductTable extends React.Component {
	render() {
		const {filterText, inStockOnly} = this.props;

		const rows = [];
		let lastCategory = null;

		this.props.products.forEach((product) => {
			if (product.name.indexOf(filterText) === -1) {
				return;
			}
			if (inStockOnly && !product.stocked) {
				return;
			}
			if (product.category !== lastCategory) {
				rows.push(
					<ProductCategoryRow
						category={product.category}
						key={product.category} />
				);
			}
			rows.push(
				<ProductRow
					product={product}
					key={product.name}
				/>
			);
			lastCategory = product.category;
		});

		return (
			<table className="table table-bordered table-striped table-hover w-25">
				<thead className="thead-dark">
				<tr>
					<th>Name</th>
					<th>Price</th>
				</tr>
				</thead>
				<tbody>{rows}</tbody>
			</table>
		);
	}
}