import React from "react";

export default class SearchBar extends React.Component {
	constructor(props) {
		super(props);

		this.searchProduct = (e) => {
			const search = e.target.value;
			this.props.productFilter(search);
		}

		this.stockCheck = (e) => {
			const checked = e.target.checked;
			this.props.toggleStock(checked);
		}
	}

	render() {

		return (
			<form>
				<input
					className="form-control w-25 mb-2"
					type="text"
					placeholder="Search..."
					onChange={this.searchProduct}
				/>
				<div className="form-check mb-3">
					<input
						className="form-check-input"
						type="checkbox"
						id="check"
						onClick={this.stockCheck}
					/>
					<label htmlFor="check" className="form-check-label">Only show products in stock</label>
				</div>
			</form>
		);
	}
}